class ParticleSystem
{
  ArrayList<ParticleEmitter> emitters;

  ParticleSystem()
  {
    this.emitters = new ArrayList<ParticleEmitter>();
  }

  void update()
  {
    for (ParticleEmitter e : this.emitters)
      e.update();
  }

  void draw()
  {
    for (ParticleEmitter e : this.emitters)
      e.draw();
  }
}

class ParticleEmitter
{
  ArrayList<Particle> particles;

  ParticleEmitter()
  {
    this.particles = new ArrayList<Particle>();
  }

  void update()
  {
    ArrayList<Particle> deadParticles = new ArrayList<Particle>();
    
    for (Particle p : this.particles)
    {
      p.update();
      if(p.dead)
        deadParticles.add(p);
    }  
    this.particles.removeAll(deadParticles);
  }

  void draw()
  {
    noFill();
    
    for (Particle p : this.particles)
    {
      color c = p.col;
      
      if(p.life <= 10)
      {
        float t = p.life / 10.0f;
        c = color(red(c), green(c), blue(c), t * 255);
      }
      stroke(c);
      ellipse(p.pos.x - p.size/2, p.pos.y - p.size/2, p.size, p.size);
    }
  }
}

class Particle extends SpaceObject
{
  color col;
  float size;
  int life;
  boolean dead;
  
  void update()
  {
    this.life--;
    if(this.life <= 0)
      this.dead = true;
      
    super.update();
  }
}
