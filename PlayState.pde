class PlayState extends GameState
{
  PlayState()
  {
    spaceObjects = new ArrayList<SpaceObject>();
    spaceObjects.add(new Ship());
    for (int i = 0; i < level; i++)
      spaceObjects.add(new Asteroid());
    
    for(SpaceObject so : spaceObjects)
    {
      if(so instanceof Ship){
        particleSystem = new ParticleSystem();
        particleSystem.emitters.add(thrusterEmitter);
      }
    }
    
    spaceObjects.add(new PowerUpShield());
  }
    
  void draw()
  {
    background(0, 0, 0);
    
    starField.draw();
    
    particleSystem.update();
    particleSystem.draw();
    
    boolean hasAsteroids = false;
  
    ArrayList<SpaceObject> temp = new ArrayList<SpaceObject>(spaceObjects);
    for (SpaceObject so : temp)
    {
      if(so instanceof Asteroid)
        hasAsteroids = true;
      
      so.update();
      if (so.dead)
      {
        if (so instanceof Ship)
        {
          highScoreKeeper.newScore(score);
          currentState = new MenuState();
        }
        spaceObjects.remove(so);
      }
      so.visit();
    }
    
    if(hasAsteroids == false)
    {
      level++;
      currentState = new PlayState();
    }
    
    textAlign(RIGHT, TOP);
    fill(0, 255, 0);
    text("SCORE " + score, width - 10, 10);
    textAlign(LEFT, TOP);
    text("LEVEL " + level, 10, 10);
  }
}
