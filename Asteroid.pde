class Asteroid extends SpaceObject
{
  ArrayList<PVector> vertices;
  float size;

  Asteroid()
  {
    this.size = 1.0f;
    //rango donde se generan los asteroides
    float r1 = height / 5;
    float r2 = height / 2;
    ////Circunferencia de spawn
    this.pos = dirVector(random(r1, r2));

    //Velocidad aleatoria del asteroide
    float velX = random(-0.1f, 0.9f);
    float velY = random(-0.1f, 0.9f);
    this.vel = new PVector(velX, velY);
    //rotacion del asteroides para q se mueva en cualquier direccion
    this.vel.rotate(random(TWO_PI));

    generate();
  }

  Asteroid(Asteroid parent)
  {
    this.pos = parent.pos.copy();
    PVector offset = dirVector(random(10.0f));
    this.pos.add(offset);
    //this.vel = dirVector(random(0.25f));
    this.vel = dirVector(random(-1.0f, 1.0f));
    //this.vel.add(parent.vel);
    
    this.size = parent.size / 2.0f;
    generate();
  }

  void draw()
  {
    pushMatrix();
    translate(this.pos.x, this.pos.y);
    stroke(random(255), random(255), random(255));
    noFill();

    beginShape();

    for (PVector v : this.vertices) 
      vertex(v.x, v.y);

    endShape(CLOSE);
    popMatrix();

    //super.update();
    //checkCollision();
  }

  void generate()
  {
    this.vertices = new ArrayList<PVector>();
    int nVertices = 12;
    float angle = 0.0f;

    for (int v = 0; v < nVertices; v++)
    {
      float fromCenter = random(20, 50) + 1.0f;
      PVector vertex = new PVector(fromCenter, 0.0f);
      vertex.rotate(angle);
      vertex.mult(this.size);
      this.vertices.add(vertex);
      angle += TWO_PI / nVertices;
    }
  }
}
