class SpaceObject
{
  PVector pos;
  PVector vel;
  
  boolean dead;
  
  void draw(){}
  
  void update()
  {
    this.pos.add(this.vel);
    if (this.pos.x < 0)
      this.pos.x += width;
    if (this.pos.y < 0)
      this.pos.y += height;
    if (this.pos.x >= width)
      this.pos.x -= width;
    if (this.pos.y >= height)
      this.pos.y -= height;
  }
  
  void visit()
  {
    draw();
    
    pushMatrix();
      translate(-width, -height);
      draw();
    popMatrix();
    
    pushMatrix();
      translate(-width, 0);
      draw();
    popMatrix();
    
    pushMatrix();
      translate(-width, height);
      draw();
    popMatrix();
    
    pushMatrix();
      translate(0, -height);
      draw();
    popMatrix();
        
    pushMatrix();
      translate(0, height);
      draw();
    popMatrix();
    
    pushMatrix();
      translate(width, -height);
      draw();
    popMatrix();
    
    pushMatrix();
      translate(width, 0);
      draw();
    popMatrix();
    
    pushMatrix();
      translate(width, height);
      draw();
    popMatrix();
  }
}
