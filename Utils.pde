//UTILIDADES
PVector dirVector(float mag, float dir)
{
  PVector v = new PVector(mag, 0);
  v.rotate(dir);

  return v;
}

PVector dirVector(float mag)
{
  return dirVector(mag, random(TWO_PI));
}
