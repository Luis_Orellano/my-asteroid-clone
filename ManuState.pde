class MenuState extends GameState
{
  MenuState()
  {
    spaceObjects = new ArrayList<SpaceObject>();
    for (int i = 0; i < 6; i++)
      spaceObjects.add(new Asteroid());
      
    starField = new StarField();
  }
  
  void keyPressed(int key)
  {
    if (key == ENTER)
    {
      score = 0;
      level = 1;
      currentState = new PlayState();
    }
  }
    
  void draw()
  {
    background(0, 0, 0);
    
    starField.draw();
  
    ArrayList<SpaceObject> temp = new ArrayList<SpaceObject>(spaceObjects);
    for (SpaceObject so : temp)
    {
      so.update();
      if (so.dead)
        spaceObjects.remove(so);   
      so.visit();
    }
    
    textAlign(CENTER, TOP);    
    fill(0, 255, 0);
    text("HIGH SCORE " + highScoreKeeper.highScore, width / 2, 10);
    
    textAlign(CENTER, BOTTOM);    
    text("PRESS ENTER TO START", width / 2, height - 10);
  }
};
