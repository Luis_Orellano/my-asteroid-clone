import processing.sound.*;
SoundFile sfxThrust, sfxExpl;

ThrusterEmitter thrusterEmitter;
ParticleSystem particleSystem;
StarField starField;

ArrayList<SpaceObject> spaceObjects;

int score;
int level;
HighScoreKeeper highScoreKeeper;

GameState currentState;

void setup()
{
  size(800, 600);
  //surface.setTitle("Asteroid");
  //surface.setResizable(true);
  
  highScoreKeeper = new HighScoreKeeper();
  
  sfxThrust = new SoundFile(this, "thrust.wav");
  sfxExpl = new SoundFile(this, "explosion.wav");
  
  PFont font = createFont("Arial Black", 24);
  textFont(font);  
  
  currentState = new MenuState();
}

void draw()
{
  currentState.draw();
}


//Keyboard
boolean upPressed = false;
boolean leftPressed = false;
boolean rightPressed = false;
boolean shootPressed = false;

void keyPressed()
{
  if (key == CODED)
  {
    if (keyCode == LEFT) leftPressed = true;
    if (keyCode == RIGHT) rightPressed = true;
    if (keyCode == UP) upPressed = true;
    if (keyCode == DOWN) shootPressed = true;
    currentState.keyPressed(keyCode);
  }
  else
    currentState.keyPressed(key);
}

void keyReleased()
{
  if (key == CODED)
  {
    if (keyCode == LEFT) leftPressed = false;
    if (keyCode == RIGHT) rightPressed = false;
    if (keyCode == UP) upPressed = false;
    if (keyCode == DOWN) shootPressed = false;
   }
}
