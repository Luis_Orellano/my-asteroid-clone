class HighScoreKeeper
{
  int highScore;
  
  HighScoreKeeper()
  {
    String[] fileLines = loadStrings("highscore.txt");
    if (fileLines == null)
    {
      this.highScore = 0;
      return;
    }
    
    this.highScore = int(fileLines[0]);
  }
  
  void newScore(int score)
  {
    if (this.highScore >= score)
      return;
      
    this.highScore = score;
    
    String[] fileLines = new String[1];
    fileLines[0] = "" + this.highScore;
    saveStrings("highscore.txt", fileLines);
  }
}
