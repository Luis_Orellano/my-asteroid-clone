final float MAX_VELOCITY = 5.0f;

class Ship extends SpaceObject
{
  float dir;
  int reloadFrames;
  int fireAmount;
  int shieldTime;

  Ship()
  {
    this.pos = new PVector(width/2, height/2);
    this.vel = new PVector(0, 0);
    this.dir = -HALF_PI;
    thrusterEmitter = new ThrusterEmitter();
    this.fireAmount = 1;
    this.shieldTime = 0;
  }

  void draw()
  {
    //nave
    pushMatrix();
      translate(this.pos.x, this.pos.y);
      noFill();
      stroke(random(255), random(255), random(255));
      rotate(this.dir);
      beginShape();
        vertex(10, 0);
        vertex(-10, -10);
        vertex(-5, 0);
        vertex(-10, 10);
      endShape(CLOSE);
      
      if(this.shieldTime > 0)
      {
        if(this.shieldTime < 60 * 2)
        {
          if((this.shieldTime % 8) < 4)
            stroke(255,255,255);
          else
            stroke(128,128,128);
        }
        else
          stroke(255,255,255);
        noFill();
        ellipse(0,0,30,30);
      }
    popMatrix();
    
    //showScore();
  }
  
  void fire()
  {
    if(this.reloadFrames > 0)
      return;
    spaceObjects.add(new Bullet(this));
    this.reloadFrames = 30;
  }
  
  void update()
  {
    if(this.reloadFrames > 0)
      this.reloadFrames--;
    if(this.shieldTime > 0)
      this.shieldTime--;
    
    if (upPressed)  
      thrust(0.1f);
    if (leftPressed) 
      turn(radians(-3));
    if (rightPressed) 
      turn(radians(3));
    if (shootPressed)
      fire();
      
    super.update();
    
    this.vel.limit(MAX_VELOCITY);
    
    for (SpaceObject so : spaceObjects)
    {
      if(this.shieldTime == 0)
      {
        if(so instanceof Asteroid)
        {
          Asteroid a = (Asteroid)so;
          boolean collided = wrappedPointInPoly(a.pos, a.vertices, this.pos);
          if(collided)
          {
            this.dead = true;
            return;
          }
        }
      }
      
      if (so instanceof PowerUp)
      {
        PowerUp pu = (PowerUp)so;   
        boolean collided = wrappedPointInCircle(pu.pos, POWER_UP_SIZE, this.pos);
        if (collided)
        {
          pu.power(this);
          pu.dead = true;
          return;
        }
      }
    }
  }
  
  void thrust(float force)
  {
    PVector f = dirVector(force, this.dir);
    this.vel.add(f);
    
    PVector offsetLeft = new PVector(14, -8);
    offsetLeft.rotate(this.dir + PI);
    offsetLeft.x += random(-2, 2);
    offsetLeft.y += random(-2, 2);
    thrusterEmitter.emit(this, offsetLeft, f);
    
    PVector offsetRight = new PVector(14, 8);
    offsetRight.rotate(this.dir + PI);
    offsetRight.x += random(-2, 2);
    offsetRight.y += random(-2, 2);
    thrusterEmitter.emit(this, offsetRight, f);
    
    if (frameCount % 2 == 0)
      sfxThrust.play();
  }

  void turn(float amount)
  {
    this.dir += amount;
  }

}


class ThrusterEmitter extends ParticleEmitter
{
  void emit(Ship ship, PVector offset, PVector force)
  {
    int particleAmount = int(force.mag() * 50);
    
    for(int p = 0; p < particleAmount; p++)
    {
      Particle newParticle = new Particle();
      newParticle.pos = new PVector(ship.pos.x, ship.pos.y);
      newParticle.pos.add(offset);
      newParticle.vel = new PVector(force.x, force.y);
      newParticle.vel.rotate(random(radians(-40), radians(40)));
      newParticle.vel.mult(-5);
      newParticle.vel.add(ship.vel);
      newParticle.life = int(floor(random(20, 40)));
      newParticle.size = int(floor(random(3, 8)));
      
      int r = int(floor(random(192, 255)));
      int g = int(floor(random(64, 128)));
      newParticle.col = color(r, g, 0);
      this.particles.add(newParticle);
    }
  }
}
