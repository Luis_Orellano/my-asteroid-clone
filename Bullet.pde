class Bullet extends SpaceObject
{
  int lifetime;
  
  Bullet(Ship ship)
  {
    this.pos = ship.pos.copy();   
    PVector offset = dirVector(10, ship.dir);
    this.pos.add(offset);
    this.vel = dirVector(5.5f, ship.dir);
    this.vel.add(ship.vel);
    this.lifetime = 120;
  }
  
  void update()
  {
    super.update();
    
    this.lifetime--;
    if (this.lifetime <= 0)
      this.dead = true;
      
    ArrayList<SpaceObject> temp = new ArrayList<SpaceObject>(spaceObjects);
    for (SpaceObject so : temp)
    {
      if (so instanceof Asteroid)
      {
        Asteroid a = (Asteroid)so;        
        boolean collided = pointInPoly(a.pos, a.vertices, this.pos);
        if (collided)
        {
          sfxExpl.play();
          a.dead = true;
          score += 100;
          if (a.size > 0.25f)
          {
            println(a.size);
            spaceObjects.add(new Asteroid(a));
            spaceObjects.add(new Asteroid(a));
          }
          this.dead = true;
          return;
        }
      }
    }
  }

  void draw()
  {
    pushMatrix();
      translate(this.pos.x, this.pos.y);
      stroke(255, 0, 0);
      noFill();
      ellipse(0, 0, 4, 4);
    popMatrix();
  }
};
