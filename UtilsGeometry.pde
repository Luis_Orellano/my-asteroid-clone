//Detecta colisiones entre poligonos..
boolean pointInPoly(PVector polyOffset, ArrayList<PVector> poly, PVector point)
{
  boolean inside = false;
  for (int i = 0; i < poly.size(); i++)
  {
    int j = i - 1;
    if (i == 0)
      j = poly.size() - 1;

    PVector iV = poly.get(i);
    PVector jV = poly.get(j);
    float iX = polyOffset.x + iV.x;
    float iY = polyOffset.y + iV.y;
    float jX = polyOffset.x + jV.x;
    float jY = polyOffset.y + jV.y;

    if ((iY < point.y && jY >= point.y) ||
        (jY < point.y && iY >= point.y))
    {
      if (iX + (point.y - iY) / (jY - iY) * (jX - iX) < point.x)
        inside = !inside;
    }
  }
  return inside;
}

boolean wrappedPointInCircle(PVector center, float radius, PVector point)
{
  if (pointInCircle(center, radius, point))
    return true;
    
  PVector p = new PVector();
  
  p.x = point.x - width;
  p.y = point.y - height;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.y = point.y;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.y = point.y + height;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.x = point.x;
  p.y = point.y - height;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.y = point.y + height;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.x = point.x + width;
  p.y = point.y - height;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.y = point.y;
  if (pointInCircle(center, radius, p))
    return true;
    
  p.y = point.y + height;
  if (pointInCircle(center, radius, p))
    return true;
    
  return false;
}

boolean pointInCircle(PVector center, float radius, PVector point)
{
  PVector distanceVector = PVector.sub(center, point);
  float distance = distanceVector.mag();
  return radius > distance;
}

boolean wrappedPointInPoly(PVector polyOffset, ArrayList<PVector> poly, PVector point)
{
  if (pointInPoly(polyOffset, poly, point))
    return true;
    
  PVector p = new PVector();
  
  p.x = point.x - width;
  p.y = point.y - height;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.y = point.y;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.y = point.y + height;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.x = point.x;
  p.y = point.y - height;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.y = point.y + height;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.x = point.x + width;
  p.y = point.y - height;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.y = point.y;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  p.y = point.y + height;
  if (pointInPoly(polyOffset, poly, p))
    return true;
    
  return false;
}
