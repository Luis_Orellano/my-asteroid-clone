class Star
{
  PVector pos;
  float size;
  
  Star()
  {
    float x = random(0, width);
    float y = random(0, height);
    this.pos = new PVector(x, y);
    this.size = random(1, 3);
  }
}

class StarField
{
  ArrayList<Star> stars;
  
  StarField()
  {
    this.stars = new ArrayList<Star>();
    
    for(int s = 0; s < 1000; s++)
      this.stars.add(new Star());
  }
  
  void draw()
  {
    stroke(255);
    for (Star s : this.stars)
    {
      strokeWeight(s.size);
      point(s.pos.x, s.pos.y);
    }
    strokeWeight(1);
  }
}
