final float POWER_UP_SIZE = 20.0f;

class PowerUp extends SpaceObject
{
  int lifeTime;
  
  PowerUp()
  {
    float distanceFromCenter = random(height /5, height / 2);
    this.pos = dirVector(distanceFromCenter);
    this.pos.add(new PVector(width / 2, height / 2));
    
    this.vel = new PVector();
    this.lifeTime = 15 * 60;
  }
  
  void power(Ship ship)
  {
  }

  void update()
  {
    super.update();
    
    this.lifeTime--;
    if(this.lifeTime <= 0)
      this.dead = true;
  }
  
  void draw()
  {
    pushMatrix();
      translate(this.pos.x, this.pos.y);
      stroke(0,255,255);
      noFill();
      ellipse(0,0, POWER_UP_SIZE, POWER_UP_SIZE);
    popMatrix();
  }
}

class PowerUpScore extends PowerUp
{
  void power(Ship ship)
  {
    score += 1000;
  }
}

class PowerUpShield extends PowerUp
{
  void power(Ship ship)
  {
    ship.shieldTime = 10 * 60;
  }
}

class PowerUpMultiFire extends PowerUp
{
  void power(Ship ship)
  {
    ship.fireAmount++;
  }
}

class PowerUpBomb extends PowerUp
{
  void power(Ship ship)
  {
    ArrayList<SpaceObject> temp = new ArrayList<SpaceObject>(spaceObjects);
    for(SpaceObject so : temp)
    {
      if(PVector.sub(so.pos, this.pos).mag() > 150.0f)
        continue;
      
      if(so instanceof Asteroid)
      {
        Asteroid a = (Asteroid)so;
        score += 100;
        if(a.size > 10.0f)
        {
          spaceObjects.add(new Asteroid(a));
          spaceObjects.add(new Asteroid(a));
        }
        a.dead = true;
      }
    }
  }
  
  void draw()
  {
    pushMatrix();
      translate(this.pos.x, this.pos.y);
      noFill();
      stroke(0, 255, 255);
      ellipse(0, 0, POWER_UP_SIZE, POWER_UP_SIZE);
      stroke(0, 128, 128);
      ellipse(0, 0, 300.0f, 300.0f);
    popMatrix();
  }
}
